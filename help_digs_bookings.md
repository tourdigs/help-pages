# Diary of Reservation Requests and Bookings

On this page you will see your TourDigs diary.

## Reservations Requests

These items are requests mae by you to stay with Host.  They have not yet been confirmed by the host, so it is not a proper booking

## Confirmed Bookings

These items are confirmed bookings. The host is expecting you! You can cancel bookings, but cancellations will show up in your profile.
> Everyone appreciates that you sometimes need to cancel, so a cancellation once in a while is acceptable.

## Past bookings

These items are previous bookings.  They will stay in the list until you have provided a rating.

### Ratings

You are encouraged to rate your host. Please provide a score out of 5 (or 0 if the Host cancelled atthe last minuted, or something else catastrophic)
and leave a comment for other to see. 