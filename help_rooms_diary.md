# Hosts Diary

In your diary you will see reservations, future bookings, blocked-out dates and your booking history.

## Reservations.

You have 48 hours to confirm or decline a reservation request.  Before then the booking is not complete, and the guest will not know your address.

> There may be multiple guest making requests for the overlapping sets of dates.  You must check through the requests and choose the ones that suit you the best. When confirming one reservation which overlaps with other requests, then these others will be automatically declined for you.

## Manage Availability.

Use the button "Manage Availability" to add dates when one or more rooms are unavailable for booking. We call this _blocking out_.
When a room is marked as _blocked out_, it will not show up in searches for these dates, and guest cannot make a booking. 

You can cancel these blocked out dates by using the delete button next to each in the confirmed dates table.

## Confirmed Diary.

In the confirmed diary, you will see all confirmed bookings and _blocked out_ dates.  