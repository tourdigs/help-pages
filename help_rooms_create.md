# Room Listing

You need to provide a room listing for each individually booked room.
These can be edited whenever you need to.

#### Address

You must first provide a postcode, then select your address.

#### Room name

This should be a short catchy name that sells your room.  

#### Room Rates

* You must provide a weekly rate and a nightly rate.  
* If you offer a discount for booking by the week, then reflect this in your pricing.
* You may also set the minimum stay duration.

> TourDigs calculates the total cost by combining the weekly and nightly rates, depending on the length of stay. E.g. for 10 days, the cost will be as 1 x weekly-rate + 3 x nightly rate.

#### Room description

Describe your room, your house, distance the Theatre by walking/bus etc. Also describe any pets you have, local ameneties, and anything else which best describes the house and the room.

#### Features

Don't forget to tick any of the features that your room/house offers.