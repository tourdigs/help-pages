# Managing Availability.

You can select dates when some or all rooms are unavailable for bookings.  We call this _blocking out_.

When you _block out_ dates, the affected rooms will not show up in searches, and will not be available for booking.

After adding _blocked out_ dates, these entries will appear in your diary where they can be deleted if necessary.

## Note

Like all other dates on TourDigs, the dates you add run overnight from midday to midday, just like check-in and check-out.  
The last date you add ends at midday, so somebody can book to stay that night.  If you don't want this, just extend the date range.