# Note about the total cost

The host may have discounts or small surcharges (laundry,breakfast,extra nights) that may not be included in the total cost shown on TourDigs.com


As the accomodation payments are handled directly between guest and host, the host will confirm the final amount.