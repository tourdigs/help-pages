## About Payment Security

We use [Stripe](https://stripe.com/about) for online payments.

Stripe is a secure, reliable and trustworthy payment system.

When you buy credits with us, we never get to see or process your credit card information. 
It is transmitted from your web-browser directly to Stripe and processed on their secure servers.
(Stripe gives us a secure link to then charge your card, when you tell us to)
    
In addition, every single bit of communication between you, us and Stripe is encrypted with indstry standard and high strength SSL certificate encryption.